<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.4" name="lava_anim" tilewidth="32" tileheight="32" tilecount="16" columns="16">
 <image source="lava_cave_anim.png" width="512" height="32"/>
 <tile id="0">
  <properties>
   <property name="animation-delay0" value="12"/>
   <property name="animation-delay1" value="12"/>
   <property name="animation-delay10" value="12"/>
   <property name="animation-delay11" value="12"/>
   <property name="animation-delay12" value="12"/>
   <property name="animation-delay13" value="12"/>
   <property name="animation-delay14" value="12"/>
   <property name="animation-delay15" value="12"/>
   <property name="animation-delay2" value="12"/>
   <property name="animation-delay3" value="12"/>
   <property name="animation-delay4" value="12"/>
   <property name="animation-delay5" value="12"/>
   <property name="animation-delay6" value="12"/>
   <property name="animation-delay7" value="12"/>
   <property name="animation-delay8" value="12"/>
   <property name="animation-delay9" value="12"/>
   <property name="animation-frame0" value="0"/>
   <property name="animation-frame1" value="1"/>
   <property name="animation-frame10" value="10"/>
   <property name="animation-frame11" value="11"/>
   <property name="animation-frame12" value="12"/>
   <property name="animation-frame13" value="13"/>
   <property name="animation-frame14" value="14"/>
   <property name="animation-frame15" value="15"/>
   <property name="animation-frame2" value="2"/>
   <property name="animation-frame3" value="3"/>
   <property name="animation-frame4" value="4"/>
   <property name="animation-frame5" value="5"/>
   <property name="animation-frame6" value="6"/>
   <property name="animation-frame7" value="7"/>
   <property name="animation-frame8" value="8"/>
   <property name="animation-frame9" value="9"/>
  </properties>
  <animation>
   <frame tileid="0" duration="120"/>
   <frame tileid="1" duration="120"/>
   <frame tileid="2" duration="120"/>
   <frame tileid="3" duration="120"/>
   <frame tileid="4" duration="120"/>
   <frame tileid="5" duration="120"/>
   <frame tileid="6" duration="120"/>
   <frame tileid="7" duration="120"/>
   <frame tileid="8" duration="120"/>
   <frame tileid="9" duration="120"/>
   <frame tileid="10" duration="120"/>
   <frame tileid="11" duration="120"/>
   <frame tileid="12" duration="120"/>
   <frame tileid="13" duration="120"/>
   <frame tileid="14" duration="120"/>
   <frame tileid="15" duration="120"/>
  </animation>
 </tile>
</tileset>
