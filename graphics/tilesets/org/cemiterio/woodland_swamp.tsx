<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.4" name="woodland_swamp" tilewidth="32" tileheight="32" tilecount="256" columns="16">
 <image source="woodland_swamp.png" width="512" height="512"/>
 <tile id="69">
  <properties>
   <property name="animation-delay0" value="32"/>
   <property name="animation-delay1" value="37"/>
   <property name="animation-delay2" value="33"/>
   <property name="animation-delay3" value="38"/>
   <property name="animation-delay4" value="40"/>
   <property name="animation-frame0" value="112"/>
   <property name="animation-frame1" value="113"/>
   <property name="animation-frame2" value="114"/>
   <property name="animation-frame3" value="115"/>
   <property name="animation-frame4" value="116"/>
  </properties>
  <animation>
   <frame tileid="112" duration="320"/>
   <frame tileid="113" duration="370"/>
   <frame tileid="114" duration="330"/>
   <frame tileid="115" duration="380"/>
   <frame tileid="116" duration="400"/>
  </animation>
 </tile>
 <tile id="71">
  <properties>
   <property name="animation-delay0" value="1200"/>
   <property name="animation-delay1" value="3"/>
   <property name="animation-delay10" value="3"/>
   <property name="animation-delay11" value="3"/>
   <property name="animation-delay12" value="3"/>
   <property name="animation-delay13" value="3"/>
   <property name="animation-delay2" value="3"/>
   <property name="animation-delay3" value="3"/>
   <property name="animation-delay4" value="3"/>
   <property name="animation-delay5" value="3"/>
   <property name="animation-delay6" value="3"/>
   <property name="animation-delay7" value="3"/>
   <property name="animation-delay8" value="3"/>
   <property name="animation-delay9" value="3"/>
   <property name="animation-frame0" value="144"/>
   <property name="animation-frame1" value="145"/>
   <property name="animation-frame10" value="148"/>
   <property name="animation-frame11" value="146"/>
   <property name="animation-frame12" value="147"/>
   <property name="animation-frame13" value="148"/>
   <property name="animation-frame2" value="146"/>
   <property name="animation-frame3" value="147"/>
   <property name="animation-frame4" value="148"/>
   <property name="animation-frame5" value="147"/>
   <property name="animation-frame6" value="146"/>
   <property name="animation-frame7" value="147"/>
   <property name="animation-frame8" value="146"/>
   <property name="animation-frame9" value="147"/>
  </properties>
  <animation>
   <frame tileid="144" duration="12000"/>
   <frame tileid="145" duration="30"/>
   <frame tileid="146" duration="30"/>
   <frame tileid="147" duration="30"/>
   <frame tileid="148" duration="30"/>
   <frame tileid="147" duration="30"/>
   <frame tileid="146" duration="30"/>
   <frame tileid="147" duration="30"/>
   <frame tileid="146" duration="30"/>
   <frame tileid="147" duration="30"/>
   <frame tileid="148" duration="30"/>
   <frame tileid="146" duration="30"/>
   <frame tileid="147" duration="30"/>
   <frame tileid="148" duration="30"/>
  </animation>
 </tile>
 <tile id="85">
  <properties>
   <property name="animation-delay0" value="40"/>
   <property name="animation-delay1" value="42"/>
   <property name="animation-delay2" value="45"/>
   <property name="animation-delay3" value="42"/>
   <property name="animation-delay4" value="40"/>
   <property name="animation-frame0" value="128"/>
   <property name="animation-frame1" value="129"/>
   <property name="animation-frame2" value="130"/>
   <property name="animation-frame3" value="131"/>
   <property name="animation-frame4" value="132"/>
  </properties>
  <animation>
   <frame tileid="128" duration="400"/>
   <frame tileid="129" duration="420"/>
   <frame tileid="130" duration="450"/>
   <frame tileid="131" duration="420"/>
   <frame tileid="132" duration="400"/>
  </animation>
 </tile>
 <tile id="86">
  <properties>
   <property name="animation-delay0" value="25"/>
   <property name="animation-delay1" value="27"/>
   <property name="animation-delay2" value="24"/>
   <property name="animation-delay3" value="27"/>
   <property name="animation-delay4" value="28"/>
   <property name="animation-frame0" value="96"/>
   <property name="animation-frame1" value="97"/>
   <property name="animation-frame2" value="98"/>
   <property name="animation-frame3" value="99"/>
   <property name="animation-frame4" value="100"/>
  </properties>
  <animation>
   <frame tileid="96" duration="250"/>
   <frame tileid="97" duration="270"/>
   <frame tileid="98" duration="240"/>
   <frame tileid="99" duration="270"/>
   <frame tileid="100" duration="280"/>
  </animation>
 </tile>
</tileset>
